export const UPDATE_QUANTITY = 'product/UPDATE_QUANTITY';
export const BUY_STEP_ONE = 'product/BUY_STEP_ONE'
export const BUY_STEP_TWO = 'product/BUY_STEP_TWO'
export const BUY_STEP_THREE = 'product/BUY_STEP_THREE'


const initialState = {
  quantity: 1,
  buyData: {
    stepOne: [{
      grind: 'cale-ziarna',
      icon: ''
    },
    {
      grind: 'kawiarka',
      icon: ''
    },
    {
      grind: 'ekspresso',
      icon: ''
    },
    {
      grind: 'aeropress',
      icon: ''
    }],
    stepTwo: [{
      id: 'xsmall',
      size: '250g',
      price: 40
    },
    {
      id: "medium",
      size: '500g',
      price: 70
    },
    {
      id: 'large',
      size: '1kg',
      price: 140
    }],
    stepThree: [{
      type: 'single'
    },{
      type: 'subscription'
    }]
  },
  grind: {},
  size: {},
  cena: {},
  type: {}
};

export default (state = initialState, action) => {
  switch (action.type) {
    case BUY_STEP_THREE:
    return { ...state, type: action.payload};

    case BUY_STEP_TWO:
      return { ...state, price: action.payload.id, size: action.payload.size, cena: action.payload.price };

    case BUY_STEP_ONE:
      return { ...state, grind: action.payload };

    case UPDATE_QUANTITY:
      return { ...state, quantity: action.payload };

    default:
      return { ...state };
  }
};

export const updateQuantity = quantity => (
  {
    type: UPDATE_QUANTITY,
    payload: quantity
  }
);

