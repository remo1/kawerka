export const FETCH_PRODUCTS_START = 'products/FETCH_PRODUCTS_START';
export const FETCH_PRODUCTS_END = 'products/FETCH_PRODUCTS_END';
export const SELECTED_ITEM = 'products/SELECTED_ITEM';

const initialState = {
  fetching: false,
  fetched: false,
  products: null,
  item: {},
  questions: [
    {
      id: 'jeden',
      question: 'Question 1',
      answer: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam pellentesque gravida ligula, ut rutrum justo faucibus at. Mauris fermentum augue eget lacus tristique, egestas convallis orci posuere. Proin ut erat pellentesque diam consequat faucibus. Morbi quis felis purus. Vestibulum ultrices semper leo, a tempor purus bibendum vitae. Quisque ultrices sed dolor ut pellentesque. Vivamus facilisis ultricies sollicitudin.',
    }, {
      id: 'dwa',
      question: 'Question 2',
      answer: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam pellentesque gravida ligula, ut rutrum justo faucibus at. Mauris fermentum augue eget lacus tristique, egestas convallis orci posuere. Proin ut erat pellentesque diam consequat faucibus. Morbi quis felis purus. Vestibulum ultrices semper leo, a tempor purus bibendum vitae. Quisque ultrices sed dolor ut pellentesque. Vivamus facilisis ultricies sollicitudin.',
    }, {
      id: 'trz',
      question: 'Question 3',
      answer: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam pellentesque gravida ligula, ut rutrum justo faucibus at. Mauris fermentum augue eget lacus tristique, egestas convallis orci posuere. Proin ut erat pellentesque diam consequat faucibus. Morbi quis felis purus. Vestibulum ultrices semper leo, a tempor purus bibendum vitae. Quisque ultrices sed dolor ut pellentesque. Vivamus facilisis ultricies sollicitudin.',
    }, {
      id: 'czte',
      question: 'Question 4',
      answer: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam pellentesque gravida ligula, ut rutrum justo faucibus at. Mauris fermentum augue eget lacus tristique, egestas convallis orci posuere. Proin ut erat pellentesque diam consequat faucibus. Morbi quis felis purus. Vestibulum ultrices semper leo, a tempor purus bibendum vitae. Quisque ultrices sed dolor ut pellentesque. Vivamus facilisis ultricies sollicitudin.',
    }
  ],
  error: null
};

export default (state = initialState, action) => {
  switch (action.type) {
    case SELECTED_ITEM:
      return {
        ...state,
        item: action.payload
      }
    case FETCH_PRODUCTS_START:
      return { ...state, fetching: true, error: null };

    case FETCH_PRODUCTS_END:
      return {
        ...state,
        fetching: false,
        fetched: true,
        products: action.payload
      };

    default:
      return { ...state, fetching: false, error: action.payload };
  }
};


export const SelectedItem = (data) => ({
  type: SELECTED_ITEM,
  payload: data
});

export const updateItem = (item) => {
  return function (dispatch) {
    dispatch(SelectedItem(item));
  };
};




export const FetchProductsStart = () => ({
  type: FETCH_PRODUCTS_START
});

export const FetchProductsEnd = data => ({
  type: FETCH_PRODUCTS_END,
  payload: data
});


export const GetProducts = resources => {
  return (dispatch, getState, api) => {
    dispatch(FetchProductsStart());

    return api.GetProducts(resources).then(products => {
      dispatch(FetchProductsEnd(products));
    });
  };
};

