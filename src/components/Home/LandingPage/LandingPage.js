import React from 'react';
import { LandingFirstPageContent } from '../LandingFirstPageContent/LandingFirstPageContent';
import { LandingContent } from '../LandingContent/LandingContent';
import { Menu } from '../../Menu/Menu';


require('./LandingPage.scss');


export const LandingPage = () => (
<div className="full-page">
    <div className="full-page--grid"> 
            <LandingFirstPageContent />
    </div>
    <div className="full-page--grid"> 
        <div className="cta-page__menu">             
             <Menu firstPageData="true" />
        </div>
        <div className="cta-page__content">
            <LandingContent />
        </div>
    </div>
</div>
);

