import React from 'react';
import Logo from '../../global/Logo/Logo';
require('./LandingFirstPageContent.scss');


export const LandingFirstPageContent = () => (
    <div className="full-page__column">
        <div className="full-page__intro">
            <Logo size="big"/>
            <div className="copy">
                <span className="cta-copy"> TO NIE KAWA. <br />TO przepyszna KAWERKA.</span>
                <p>Przygotowana w taki sposob jaki najbardziej lubisz. Kawerka swiezo prazona z ziaren etycznie pozyskiwanych.</p>
                <a className="button first-page-cta" href='asd'> poka mnie kawerke </a>
            </div>
        </div>
    </div>
);

