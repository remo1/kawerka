import React from 'react';
import coffee from '../../../img/coffee-4.jpg';

require('./LandingContent.scss');


export const LandingContent = () => (
    <div className="landing">
        <div className="landing-img">
            <img src={coffee} alt="coffee"/>
        </div>
        <div className="landing-copy">  
            <span className="cta-copy">all of our coffees come with free 1st class delivery so what are you waiting for? Awesome coffee is just a few clicks away.</span>
            <p>We roast and post daily in whole bean or your choice of grind size, so why not grab a bag today or start a subscription so you never run out of Two Chimps coffee again! And don't forget, all of our coffees come with free 1st class delivery so what are you waiting for? Awesome coffee is just a few clicks away.</p>
            <p>Buy coffee in our <a className="button-text" href='/shop'>shop</a> or scroll down to start your subscription today.</p>
        </div>
    </div>
);

