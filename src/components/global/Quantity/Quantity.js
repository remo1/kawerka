import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as api from '../../../utils/moltin';

import { UPDATE_QUANTITY } from '../../../ducks/product';

import {
    FETCH_CART_START,
    FETCH_CART_END,
    CART_UPDATED
} from '../../../ducks/cart';
require('./Quantity.scss')

const mapStateToProps = state => {
    return { ...state }
};

class Quantity extends Component {
    render() {
        var updateQuantity = (id, quantity) => {
            console.log(this.props)

            this.props.dispatch(dispatch => {
                api
                    .UpdateCart(id, quantity)

                    .then(cart => {
                        console.log(cart);
                        dispatch({ type: CART_UPDATED, gotNew: false });
                    })

                    .then(() => {
                        dispatch({ type: FETCH_CART_START, gotNew: false });

                        api
                            .GetCartItems()

                            .then(cart => {
                                dispatch({ type: FETCH_CART_END, payload: cart, gotNew: true });
                            });
                    })
                    .catch(e => {
                        console.log(e);
                    });
            });
        };
        return (
            <div className="quantity-cart">
                <form className="product" noValidate>
                    <div className="quantity-input">
                        <button
                            type="button"
                            className="decrement number-button"
                            onClick={() => {
                                updateQuantity(this.props.item_id.id, (this.props.item_id.quantity - 1));
                            }}>
                            <span className="hide-content">Decrement quantity</span>
                            <span aria-hidden="true">-</span>
                        </button>
                        <input
                            className="quantity"
                            name="text"
                            type="text"
                            min="1"
                            max="10"
                            value={this.props.item_id.quantity}
                            size="2"
                            onChange={event => {
                                updateQuantity(this.props.item_id.id, event.target.value);
                            }}
                        />
                        <button
                            type="button"
                            className="increment number-button"
                            onClick={() => {
                                updateQuantity(this.props.item_id.id, (this.props.item_id.quantity + 1));
                            }}>
                            <span className="hide-content">Increment quantity</span>
                            <span aria-hidden="true">+</span>
                        </button>
                    </div>
                </form>
            </div>
        );
    }
}

export default connect(mapStateToProps)(Quantity);
