

import React from 'react';
import { Link } from 'react-router-dom';


require('./logo.scss');

export const Logo = (props) => (
    <Link to="/" >
    <div className="logo">
                <svg id={props.size}>
                    <use href={props.size === "big" ? '#logo-svg' : '#logo-menu'}></use>
                </svg>
        </div>
    </Link>
);

export default Logo;