import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { ProductImage } from '../Products/AllProducts/ProductImage';
import { Menu } from '../Menu/Menu'
import  Quantity  from '../global/Quantity/Quantity'
import { GetProducts } from '../../ducks/products';
import { GetCartItems } from '../../ducks/cart';
 
require('./Cart.scss')

class Cart extends Component {
  componentWillMount() {
    const script = document.createElement('script');

    script.src = '../../js/production.min.js';
    script.async = false;

    document.body.appendChild(script);
  }

  componentDidMount() {
    this.props.GetProducts();
    this.props.GetCartItems();
  }

  render() {
    const { cart, products } = this.props;
    console.log(this.props)
    if (
      cart.fetched === true &&
      cart.fetching === false && 
      products.fetched === true

    ) {
      console.log(cart);
      if (cart.cart.data[0]) {
        // var subtotal = cart.cart.data[0].meta.display_price.with_tax.unit.amount / 100 + 'zl';
        return (
          <div className="full-page">
            <div className="full-page--grid">
              <div className="cta-page__menu">
                <Menu menuRender={this.props.item} />
              </div>
              <div className="product-first-page kup">
                <main role="main" id="container" className="main-container push">
                  <section className="products-cart">
                    <div className="cta-page__menu">
                      <Menu menuRender={this.props.item} />
                    </div>
                    {cart.cart.data.map(item => {
                      return (
                        <a
                          className="product-item"
                          key={item.id}>
                          <div className="product-image">
                            <ProductImage cartItems="true" product={item.id} products={item} />
                          </div>
                          <div className="overlay">
                            <div className="overlay-content">
                              <div className="title">{item.name}</div>
                              <div className="price"> <Quantity item_id={item} /> </div>
                              <div className="price">Cena: {item.unit_price.amount / 100 * item.quantity} zł</div>
                            </div>
                          </div>
                        </a>
                      );
                    })}
                  </section>
                </main>
              </div>
            </div>
          </div>


        )
      } else {
        return (
          <div> else </div>
        );
      }
    } else {
      return (
        <div> loading </div>
      );
    }
  }
}

const mapStateToProps = ({ products, cart }) => ({
  products,
  cart
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      GetProducts,
      GetCartItems
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(Cart);
