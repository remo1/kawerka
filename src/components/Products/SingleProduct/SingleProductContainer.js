import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Menu } from '../../Menu/Menu'
import SingleProduct from './SingleProduct/SingleProduct';
import { GetProducts } from '../../../ducks/products';
import { updateItem } from '../../../ducks/products';


import BuyItem from './BuyItem/BuyItem'
import PrawoKawerki from './PrawoKawerki/PrawoKawerki'
import KawerkaPomoze from './KawerkaPomoze/KawerkaPomoze'

require('./SingleProductContainer.scss')

class Product extends Component {
    componentDidMount() {
        const { fetched } = this.props;
        if (!fetched) {
            this.props.GetProducts();
        }
    }
    componentDidUpdate() {
        this.getProducts();
    }
    getProducts() {
        let id = this.props.location.pathname.replace('/product/', '');
        let selectedProduct = this.props.products.data.filter((item) => { return item.id === id });
        this.props.updateItem(selectedProduct[0]);
    }
    render() {
        const { products } = this.props;
        if (products) {

            return (
                <div>
                    <div className="full-page">
                        <div className="full-page--grid">
                            <div className="cta-page__menu">
                                <Menu menuRender={this.props.item} />
                            </div>
                            <BuyItem />


                        </div>
                    </div>

                    <div className="full-page">
                        <div className="full-page--grid">
                            <PrawoKawerki />
                        </div>
                    </div>

                    <div className="full-page">
                        <div className="full-page--grid">
                            <KawerkaPomoze />
                        </div>
                    </div>

                    <div className="full-page">
                        <div className="full-page--grid">
                        <SingleProduct />

                        </div>
                    </div>
                </div>

            );
        } else {
            return (
                <div>
                </div>
            );
        }
    }
}

const mapStateToProps = ({ products: { fetched, products, item, questions } }) => {
    return { fetched, products, item, questions }
};

const mapDispatchToProps = dispatch =>
    bindActionCreators(
        {
            GetProducts,
            updateItem
        },
        dispatch
    );

export default connect(mapStateToProps, mapDispatchToProps)(Product);
