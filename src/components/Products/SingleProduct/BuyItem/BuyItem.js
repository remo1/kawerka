import React, { Component } from 'react';
import { connect } from 'react-redux';
import ProductImage from '../..//AllProducts/ProductImage';
import ItemInfo from './ItemInfo';

const mapStateToProps = state => {
  return state;
};

class BuyItem extends Component {
  render() {
    var products = this.props.products.products;
    var ID = this.props.router.location.pathname.slice(9, 100);
    var productArray = this.props.products.products.data.filter(function (
      product
    ) {
      return product.id === ID;
    });
    var product = productArray[0];

    return (
      <div className="product-first-page kup">
        <main role="main" id="container" className="main-container push">
          <section className="product-grid product-info">
              <ProductImage
                product={product}
                products={products}
              />
              <ItemInfo />
          </section>
        </main>
      </div>
    );
  }
}

export default connect(mapStateToProps)(BuyItem);
