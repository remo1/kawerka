import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as api from '../../../../utils/moltin';
import { UPDATE_QUANTITY } from '../../../../ducks/product';
import {
  FETCH_CART_START,
  FETCH_CART_END,
  CART_UPDATED
} from '../../../../ducks/cart';

import BuyAction from './BuyAction'
require('./ItemInfo.scss')

const mapStateToProps = state => {
  return state;
};

class ItemInfo extends Component {
  render() {
    var products = this.props.products.products;

    var ID = this.props.router.location.pathname.slice(9, 100);

    var productArray = this.props.products.products.data.filter(function (
      product
    ) {
      return product.id === ID;
    });

    var product = productArray[0];

    var updateQuantity = quantity => {
      this.props.dispatch(dispatch => {
        dispatch({ type: UPDATE_QUANTITY, payload: quantity });
      });
    };

    var addToCart = id => {
      this.props.dispatch(dispatch => {
        api
          .AddCart(id, this.props.product.quantity)

          .then(cart => {
            console.log(cart);
            dispatch({ type: CART_UPDATED, gotNew: false });
          })

          .then(() => {
            dispatch({ type: FETCH_CART_START, gotNew: false });

            api
              .GetCartItems()

              .then(cart => {
                dispatch({ type: FETCH_CART_END, payload: cart, gotNew: true });
              });
          })
          .catch(e => {
            console.log(e);
          });
      });
    };


    function isThereACurrencyPrice() {
      try {
        return (
          <p className="price">
            <span className="hide-content">Cena od: {product.meta.display_price.with_tax.amount / 100 + 'zl'} </span>
            {/* {isThereACurrencyPrice()} */}
          </p>
        );
      } catch (e) {
        return <div className="price">Price not available</div>;
      }
    }

    return (
       
        <section className="buy">
          <h2>{product.name}</h2>
          <div className="buy-description">
            <p>{product.description}</p>
          </div>
          
            <BuyAction />
            

                    {/* <form className="product" noValidate>
                  <div className="quantity-input">


                  <button
                    type="submit"
                    className="submit"
                    onClick={e => {
                      addToCart(product.id);
                      console.log(this.props.product.quantity);
                      e.preventDefault();
                    }}>
                    Add to cart
                  </button>
                </form> */}

        </section>
    );
  }
}

export default connect(mapStateToProps)(ItemInfo);
