
import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as api from '../../../../utils/moltin';

import { BUY_STEP_ONE } from '../../../../ducks/product';
import { BUY_STEP_TWO } from '../../../../ducks/product';
import { BUY_STEP_THREE } from '../../../../ducks/product';
import {
    FETCH_CART_START,
    FETCH_CART_END,
    CART_UPDATED
} from '../../../../ducks/cart';

require('./BuyAction.scss')


const mapStateToProps = state => {
    return state;
};

class BuyAction extends Component {

    removePag() {
        let pag = document.querySelectorAll('.b-grind__pagination span');
        pag.forEach(i => {
            i.classList.remove('selected')
        })
    }
    removeSelecter(id) {
        let itemArray = document.querySelectorAll('.' + id + ' .b-grind__item');
        itemArray.forEach(i => {
            i.classList.remove('selected')
        })



    }

    render() {

        var products = this.props.products.products;

        var ID = this.props.router.location.pathname.slice(9, 100);

        var productArray = this.props.products.products.data.filter(function (
            product
        ) {
            return product.id === ID;
        });

        var product = productArray[0];
        //console.log(product)
        var pagination = (step) => {
            let items = document.querySelectorAll('.b-grind')
            items.forEach(item => {
                item.classList.remove('show', 'hide');
            })
            document.querySelector('.' + step).classList.add('show')
            this.removePag();

        }

        var firstStep = (item, type) => {
            this.removeSelecter('first');
            this.removePag();
            document.querySelector('#' + item.grind.replace('-', '')).classList.add('selected')
            document.querySelector('.first').classList.add('hide')
            var sibling = document.querySelector('.first').nextSibling;
            sibling.classList.add('show')
            document.querySelector('.two').classList.add('selected')
            this.props.dispatch(dispatch => {
                dispatch({ type: BUY_STEP_ONE, payload: item.grind });
            });
        };


        var secondStep = (id, size, price) => {
            this.removeSelecter('second');
            this.removePag();
            document.querySelector('#' + id).classList.add('selected')
            document.querySelector('.second').classList.add('hide')
            document.querySelector('.three').classList.add('selected')
            var sibling = document.querySelector('.second').nextSibling;
            sibling.classList.add('show')
            this.props.dispatch(dispatch => {
                dispatch({ type: BUY_STEP_TWO, payload: { id, size, price } });
            });
        }


        var thirdStep = (item) => {
            this.removeSelecter('third');
            this.removePag();
            this.props.dispatch(dispatch => {
                dispatch({ type: BUY_STEP_THREE, payload: item });
            });
            document.querySelector('.one').classList.add('hide')
        };

        let addToCart = () => {
            console.log(this.props);

            const cartItem = {
                item: this.props.products.item,
                quantity: this.props.product.quantity,
                grind: this.props.product.grind,
                size: this.props.product.size,
                price: {
                    amount: this.props.product.cena
                }
            }

            console.log(cartItem)

            this.props.dispatch(dispatch => {
                api
                    .AddCart(product.id, product.quantity)
                    .then(cart => {
                        console.log(cart);
                        dispatch({ type: CART_UPDATED, gotNew: false });
                    })
                    .then(() => {
                        dispatch({ type: FETCH_CART_START, gotNew: false });
                        api
                            .GetCartItems()
                            .then(cart => {
                                dispatch({ type: FETCH_CART_END, payload: cart, gotNew: true });
                            });
                    })
                    .catch(e => {
                        console.log(e);
                    });
            });

        };



        return (
            <div>
                {/* */}
                <div className="b-action">

                    <div className="b-grind first">
                        {this.props.product.buyData.stepOne.map(item => {
                            return (
                                <div key={item.grind} id={item.grind.replace('-', '')} onClick={() => firstStep(item)} className="b-grind__item">
                                    <span> {item.icon} </span>
                                    <span> {item.grind.replace('-', ' ')} </span>
                                </div>
                            )
                        })}
                    </div>


                    <div className="b-grind second">
                        {this.props.product.buyData.stepTwo.map(item => {
                            return (
                                <div key={item.size} id={item.id} onClick={() => secondStep(item.id, item.size, item.price)} className="b-grind__item">
                                    <span> {item.size} </span>
                                    <span> {item.price} </span>
                                </div>
                            )
                        })}
                    </div>

                    <div className="b-grind third">
                        {this.props.product.buyData.stepThree.map(item => {
                            return (
                                <div key={item.type} id={item.type} onClick={(e) => {
                                    thirdStep(item.type)
                                    addToCart()
                                    e.preventDefault()
                                }} className="b-grind__item" >
                                    <span > {item.type} </span>
                                </div>
                            )
                        })}
                    </div>
                </div>
                <div className="b-grind b-grind__pagination">
                    <span onClick={() => pagination('first')} className="one selected"> 1</span>
                    <span onClick={() => pagination('second')} className="two"> 2</span>
                    <span onClick={() => pagination('third')} className="three"> 3</span>
                </div>
            </div >

        );
    }
}

export default connect(mapStateToProps)(BuyAction);
