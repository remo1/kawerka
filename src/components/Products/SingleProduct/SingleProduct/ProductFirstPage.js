import React from 'react';
import ProductImage from '../..//AllProducts/ProductImage';

require('./ProductFirstPage.scss');


export const ProductFirstPage = (props) => (
    <div className="intro-copy">
        <h1 className="cta-copy intro-copy-big"> {props.productName} </h1>
        <span className="cta-copy intro-copy-medium"> TO NIE KAWA. < br/> TO przepyszna KAWERKA.</span>
        <p>Wyborna Kawerka. Recznie prazona na kazde zamowienie.</p>
        <a className="button " href='asd'> kup </a>
        <a className="button lines " href='asd'> zobacz wiecej </a>
    </div>
);

