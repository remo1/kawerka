import React, { Component } from 'react';
import { connect } from 'react-redux';
import ItemInfo from './ItemInfo';

const mapStateToProps = state => {
  return state;
};

class PrawoKawerki extends Component {
  render() {

    return (
      <div className="product-first-page kup">
        <div id="container" className="main-container push">
          <section className="product-grid">
            <div className="intro">
              <h1>PRAWO KAWERKI</h1>
            </div>
            <div className="prawo">
              <ItemInfo />
            </div>
          </section>
        </div>
      </div>
    );
  }
}

export default connect(mapStateToProps)(PrawoKawerki);
