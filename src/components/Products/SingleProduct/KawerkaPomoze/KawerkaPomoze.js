import React, { Component } from 'react';
import { connect } from 'react-redux';
import ItemInfo from './ItemInfo';

const mapStateToProps = ({ products: { item } }) => {
  return { item }
};

class KawerkaPomoze extends Component {
  render() {
    return (
      <div className="product-first-page kup">
        <div id="container" className="main-container push">
          <section className="product-grid">

            <div className="intro">
              <h1>Kawerka zawsze pomoŻe</h1>
            </div>
            <ItemInfo />
          </section>
        </div>
      </div>
    );
  }
}

export default connect(mapStateToProps)(KawerkaPomoze);
