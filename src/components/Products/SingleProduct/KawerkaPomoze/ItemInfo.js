import React, { Component } from 'react';
import { connect } from 'react-redux';

const mapStateToProps = ({ products: { questions } }) => {
  return { questions }
};

require('./ItemQuestion.scss')

class ItemInfo extends Component {
  open() {

  }
  render() {
    console.log(this.props)
    let obj = this.props.questions
    return (
      <div className="prawo pomoze">
        <ul className="q">
          {Object.keys(obj).map(function (key) {
            return (
              <li className="q-container" key={key}>
                <span className="q-icon"><svg><use href="#coffee-cup"></use></svg></span>
                <div className="q-info">
                  <input className="toggle-box" id={obj[key].id} type="checkbox" />
                  <label className="q-question" htmlFor={obj[key].id} >{obj[key].question}</label>
                  <div className="q-answer">{obj[key].answer} </div>
                </div>
              </li>
            )
          })}
        </ul>
      </div>
    )
  }
}

export default connect(mapStateToProps)(ItemInfo);
