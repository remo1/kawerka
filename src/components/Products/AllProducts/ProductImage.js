import React from 'react';
require('./ProductImage.scss');

export const ProductImage = props => {
  if(props.cartItems === 'true'){
    console.log(props)
    return (
      (
        <img
          alt={props.name + '-' + props.description}
          src={props.products.image.href}
          style={{ background: props.background }}
        />
      ) || <img alt="placeholder"  />
    );

  }else{

  let file;
  let fileId;
  let placeholder =
    'https://placeholdit.imgix.net/~text?txtsize=69&txt=824%C3%971050&w=824&h=1050';

  var isThereAMainImage = product => {
    fileId = props.product.relationships.main_image.data.id;

    file = props.products.included.main_images.find(function(el) {
      return fileId === el.id;
    });

    return (
      (
        <img
          alt={props.product.name + '-' + props.product.description}
          src={file.link.href}
          style={{ background: props.background }}
        />
      ) || <img alt="placeholder" src={placeholder} />
    );
  };
  
  }

  var isThereAFile = product => {
    let file;
    let fileId;
    let placeholder =
    'https://placeholdit.imgix.net/~text?txtsize=69&txt=824%C3%971050&w=824&h=1050';
    try {
      fileId = props.product.relationships.files.data[0].id;
      file = props.products.included.files.find(function(el) {
        return fileId === el.id;
      });
      return (
        <img
          alt={props.product.name + ', ' + props.product.description}
          src={file.link.href}
          style={{ background: props.background }}
        />
      );
    } catch (e) {
      return <img alt="placeholder" src={placeholder} />;
    }
  };

  try {
    return isThereAMainImage(props);
  } catch (e) {
    return isThereAFile(props);
  }
};

export default ProductImage;
