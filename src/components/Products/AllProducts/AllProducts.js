import React from 'react';
import { connect } from 'react-redux';
import { ProductImage } from './ProductImage';


require('./AllProducts.scss');

const isThereACurrencyPrice = product => {
  try {
    return (
      <div className="price">
        <div className="price-copy">Cena od: </div>
        <span>{product.meta.display_price.with_tax.amount / 100} zł</span>
      </div>
    );
  } catch (e) {
    return <div className="price">Price not available</div>;
  }
};

const AllProducts = props => {
  if (props.css !== null && props.products.products.data.length > 0) {
    var products = props.products.products;

    return (
      <div className="product-first-page kup">
        <main role="main" id="container" className="main-container push">
          <section className="products">
                {products.data.map(function (product) {
                  return (
                    <a
                      className="product-item"
                      href={'/product/' + product.id}
                      key={product.id}>
                      <div className="product-image">
                        <ProductImage product={product} products={products} />
                      </div>
                      <div className="overlay">
                        <div className="overlay-content">
                          <div className="title">{product.name}</div>
                          {isThereACurrencyPrice(product)}
                        </div>
                      </div>
                    </a>
                  );
                })}
          </section>
        </main>
      </div>
    );
  }

  return (
    <main role="main" id="container" className="main-container push">
      <section className="products">
        <div className="content">
          <p>You do not have any products</p>
        </div>
      </section>
    </main>
  );
};


const mapStateToProps = ({ products }) => ({
  products
});


export default connect(mapStateToProps)(AllProducts);
