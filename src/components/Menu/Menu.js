import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import Logo from '../global/Logo/Logo';
import { connect } from 'react-redux'
require('./Menu.scss')

const mapStateToProps = state => {
    return {
        ...state
    };
};

export class Menu extends Component {
    getTitle(name) {
        return name;
    }

    shopJourney() {
        if (this.props.menuRender) {
            return (
                <ul>
                    <li><NavLink activeClassName="selected-second" to={this.props}>{this.props.menuRender.name}</NavLink></li>
                    <li><NavLink activeClassName="selected" className="sub-item" to="/products">prawo kawerki</NavLink></li>
                    <li><NavLink activeClassName="selected" className="sub-item" to="/products">Kawerka zawsze pomoze</NavLink></li>
                    <li><NavLink activeClassName="selected" className="sub-item" to="/products">kup</NavLink></li>
                </ul>
            )
        }
    }
    firstLink() {
        if (!this.props.menuRender && !this.props.shopMenu) {
            return <li><NavLink to="/products">sklep</NavLink></li>
        } else {
            return <li><NavLink className="selected-first" to="/products">sklep</NavLink></li>
        }
    }
    firstPage() {
        if (!this.props.firstPageData) {
            return "pos-fixed"
        } else {
            return "pos-absolute" 
        }
    }
    render() {
        return (
            <div className={this.firstPage()}>
                <Logo size="small" />
                <nav>
                    <ul className="master-nav">
                        {this.firstLink()}
                        <li><NavLink to="/subskrypcja">subskrypcja</NavLink></li>
                        <li><NavLink to="/cart">koszyk</NavLink></li>
                        <li><NavLink to="/kontakt">kontakt</NavLink></li>
                    </ul>
                        {this.shopJourney()}
                </nav>
            </div>
        )
    }
}

export default connect(mapStateToProps)(Menu);